# test-task-market-holidays

### Notes

- Rows in the file are considered to be ordered by `Date` field.
- Data flow assumed to be very big or maybe even "online". `Iterator` was chosen as an interface which can represent infinite flow of dates.
- The idea here is to have some kind of merge-join between the dates flow from file and an incrementing working dates flow. The complexity for the merge join is `O(N*Log(N) + M*Log(M))`, but since we are not doing any sorting the `Log(N) = Log(M) = 1` and `M` is actually generated, roughly saying we need to read the source only once and have not extra "indexes".
- If the input dates are not sorted, my solution wouldn't work. In this case we are not able to stream the dates as we never can be sure that the "missing date" wouldn't appear with the next message. We can apply something like hash-join (rush draft example can be found in DummyHashTableFilterServiceImpl.java as a replacement for MergeJoinHolidaysFilterService.java).
- Not everything is perfectly covered with unit tests.

### Run app

```shell
./gradlew bootRun
```

by default, it pickups `input.csv` file in root folder.

### Build with tests

```shell
./gradlew build
```