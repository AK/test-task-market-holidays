package testtask.marketholidays;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import testtask.marketholidays.service.consumer.DatesConsumer;
import testtask.marketholidays.service.filter.HolidaysFilterService;
import testtask.marketholidays.service.provider.DatesProvider;

@Slf4j
@SpringBootApplication
public class Application {

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Bean
	public CommandLineRunner run(DatesProvider datesProvider, HolidaysFilterService holidaysFilterService, DatesConsumer datesConsumer) {
		return args -> {
			datesConsumer.acceptAll(
					holidaysFilterService.applyFiler(
							datesProvider.iterator()
					)
			);
		};
	}
}
