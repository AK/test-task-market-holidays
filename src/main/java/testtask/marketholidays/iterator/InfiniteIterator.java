package testtask.marketholidays.iterator;

import java.util.Iterator;

/**
 * Iterator which always has next element.
 */
public interface InfiniteIterator<E> extends Iterator<E> {
    @Override
    default boolean hasNext() {
        return true;
    }
}
