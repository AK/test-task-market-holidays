package testtask.marketholidays.iterator;

import java.util.function.Function;

/**
 * Iterator which provides every next element as a result of the function to which the previous element was given.
 */
public class InfiniteLoopingFunctionIterator<E> implements InfiniteIterator<E> {
    private final Function<E, E> incrementFunction;
    private E value;

    public InfiniteLoopingFunctionIterator(E initialValue, Function<E, E> incrementFunction) {
        this.incrementFunction = incrementFunction;
        this.value = initialValue;
    }

    @Override
    public E next() {
        E returnValue = value;
        value = incrementFunction.apply(value);
        return returnValue;
    }
}
