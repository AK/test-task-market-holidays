package testtask.marketholidays.iterator;

import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;
import com.google.common.collect.UnmodifiableIterator;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Provides iterator for the elements which are not merge-joined (left element does not have a pair on the right or vice versa) for two sorted iterators.
 * In terms of the SQL physical operations it could be expresses as "FULL OUTER MERGE JOIN".
 * This iterator is treated as one which is reached the end when either left or right iterator is reached the end,
 * it means that trailing elements of the not finished iterator are ignored and not reflected as difference of the merge.
 * Precondition: both {@code leftIterator} and {@code rightIterator} should be sorted according to {@code elementComparator} with ascending direction.
 */
public class MergeSortedAscOuterIterator<E> extends UnmodifiableIterator<E> {

    private final PeekingIterator<E> leftIterator;
    private final PeekingIterator<E> rightIterator;
    private final Comparator<E> elementComparator;
    private boolean hasNextElement = true;

    public MergeSortedAscOuterIterator(Iterator<E> leftIterator, Iterator<E> rightIterator, Comparator<E> elementComparator) {

        this.leftIterator = Iterators.peekingIterator(
               leftIterator
        );
        this.rightIterator = Iterators.peekingIterator(
            rightIterator
        );
        this.elementComparator = elementComparator;

        if (!leftIterator.hasNext() || !rightIterator.hasNext()) {
            hasNextElement = false;
            return;
        }

        skipEqualPairs();
    }

    @Override
    public boolean hasNext() {
        if (hasNextElement) {
            return true;
        }
        return false;
    }

    @Override
    public E next() {
        if (!hasNextElement) {
            throw new NoSuchElementException();
        }
        E nextItem = elementComparator.compare(leftIterator.peek(), rightIterator.peek()) > 0
                ? advance(rightIterator)
                : advance(leftIterator);
        // sink iterators to the next mismatch
        skipEqualPairs();
        return nextItem;
    }

    /**
     * Skips elements which from left and right iterators are equal each other.
     */
    private void skipEqualPairs() {
        while (hasNextElement && elementComparator.compare(leftIterator.peek(), rightIterator.peek()) == 0) {
            advance(leftIterator);
            advance(rightIterator);
        }
    }

    private E advance(Iterator<E> iterator) {
        E nextElement = iterator.next();
        boolean hasNext = iterator.hasNext();
        if (!hasNext) {
            hasNextElement = false;
        }
        return nextElement;
    }
}
