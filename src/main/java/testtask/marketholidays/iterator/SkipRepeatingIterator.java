package testtask.marketholidays.iterator;

import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;

import java.util.Iterator;
import java.util.Objects;

/**
 * Skips equal consecutive elements.
 */
public class SkipRepeatingIterator<E> implements Iterator<E> {

    private final PeekingIterator<E> iterator;

    public SkipRepeatingIterator(Iterator<E> iterator) {
        this.iterator = Iterators.peekingIterator(iterator);
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public E next() {
        E next = iterator.next();
        while (iterator.hasNext() && Objects.equals(next, iterator.peek())) {
            next = iterator.next();
        }
        return next;
    }
}
