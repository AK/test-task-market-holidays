package testtask.marketholidays.service.consumer;

import java.time.LocalDate;
import java.util.Iterator;
import java.util.function.Consumer;

public interface DatesConsumer extends Consumer<LocalDate> {
    default void acceptAll(Iterator<LocalDate> iterator) {
        while (iterator.hasNext()) {
            accept(iterator.next());
        }
    }
}
