package testtask.marketholidays.service.consumer;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Slf4j
@Service
public class LoggerDatesConsumer implements DatesConsumer {
    @Override
    public void accept(LocalDate localDate) {
        log.info("Holiday at {}", localDate);
    }
}
