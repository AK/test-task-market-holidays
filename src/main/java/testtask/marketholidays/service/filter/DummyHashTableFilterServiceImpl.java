package testtask.marketholidays.service.filter;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;
//
// just some quick and rubbish implementation via hash tables.
// see MergeJoinHolidaysFilterService for more appropriate version
// btw, it does not pass the tests because it is not stable sort and result is shuffled
// apart from that it seems to be working as well
//
public class DummyHashTableFilterServiceImpl implements HolidaysFilterService {
    @Override
    public Iterator<LocalDate> applyFiler(Iterator<LocalDate> inputDates) {
        Set<LocalDate> uniqueDates = new HashSet<>();

        // find min/max and pull all dates to hash table
        LocalDate min = null;
        LocalDate max = null;
        while (inputDates.hasNext()) {
            LocalDate date = inputDates.next();
            uniqueDates.add(date);
            if (min == null || date.isBefore(min)) {
                min = date;
            }

            if (max == null || date.isAfter(max)) {
                max = date;
            }
        }

        // walk through all the working dates between min and max
        // and lookup hash table
        Set<LocalDate> resultDates = new HashSet<>();
        LocalDate date = min;
        while (date.isBefore(max)) {
            date = date.plusDays(1);
            if (date.getDayOfWeek() != DayOfWeek.SATURDAY
                    && date.getDayOfWeek() != DayOfWeek.SUNDAY
                    && !uniqueDates.contains(date)
            ) {
                resultDates.add(date);
            }
        }

        return resultDates.iterator();
    }
}
