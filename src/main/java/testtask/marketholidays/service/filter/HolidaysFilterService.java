package testtask.marketholidays.service.filter;

import java.time.LocalDate;
import java.util.Iterator;

public interface HolidaysFilterService {
    Iterator<LocalDate> applyFiler(Iterator<LocalDate> inputDates);
}
