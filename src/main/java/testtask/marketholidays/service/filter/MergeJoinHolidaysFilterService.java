package testtask.marketholidays.service.filter;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterators;
import com.google.common.collect.PeekingIterator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import testtask.marketholidays.iterator.InfiniteLoopingFunctionIterator;
import testtask.marketholidays.iterator.MergeSortedAscOuterIterator;
import testtask.marketholidays.iterator.SkipRepeatingIterator;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.Iterator;

@Slf4j
@Service
@RequiredArgsConstructor
public class MergeJoinHolidaysFilterService implements HolidaysFilterService {
    @Override
    public Iterator<LocalDate> applyFiler(Iterator<LocalDate> inputDates) {
        PeekingIterator<LocalDate> inputDatesIterator = Iterators.peekingIterator(
                new SkipRepeatingIterator<>(inputDates)
        );

        LocalDate firstDate = inputDatesIterator.peek();
        Iterator<LocalDate> plusOneDayIterator =
                Iterators.filter(
                        new InfiniteLoopingFunctionIterator<>(firstDate, date -> date.plusDays(1)),
                        date -> !date.getDayOfWeek().equals(DayOfWeek.SATURDAY) && !date.getDayOfWeek().equals(DayOfWeek.SUNDAY)
                );

        return new MergeSortedAscOuterIterator<>(
                inputDatesIterator,
                plusOneDayIterator,
                getLocalDateComparator()
        );
    }

    private Comparator<LocalDate> getLocalDateComparator() {
        return (date1, date2) -> {
            if (date1.equals(date2)) return 0;
            if (date1.isAfter(date2)) return 1;
            return -1;
        };
    }
}
