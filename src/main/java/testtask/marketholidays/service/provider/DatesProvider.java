package testtask.marketholidays.service.provider;

import java.time.LocalDate;

public interface DatesProvider extends Iterable<LocalDate> {
}
