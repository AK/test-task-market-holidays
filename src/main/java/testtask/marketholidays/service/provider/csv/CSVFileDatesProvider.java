package testtask.marketholidays.service.provider.csv;

import com.google.common.collect.Iterators;
import com.opencsv.CSVReader;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import testtask.marketholidays.service.provider.DatesProvider;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;

@Slf4j
@Service
public class CSVFileDatesProvider implements DatesProvider {
    @Value("${input-file.path:input.csv}")
    private String inputFile;
    @Value("${input-file.date-format:yyyy-MM-dd}")
    private String dateFormatPattern;
    @Value("${input-file.date-column-index:0}")
    private int dateColumnIndex;
    @Value("${input-file.number-of-lines-to-skip:1}")
    private int numberOfLinesToSkip;

    @Override
    @SneakyThrows
    public Iterator<LocalDate> iterator() {
        File file = new File(inputFile);
        if (!file.exists()) {
            log.error("Input file {} not found", file.getAbsolutePath());
        }

        CSVReader csvReader = new CSVReader(new BufferedReader(new FileReader(file)));
        csvReader.skip(numberOfLinesToSkip);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormatPattern);

        return Iterators.transform(csvReader.iterator(), (columns) -> LocalDate.parse(columns[dateColumnIndex], dateTimeFormatter));
    }
}
