package testtask.marketholidays;

import com.google.common.collect.Lists;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import testtask.marketholidays.service.consumer.DatesConsumer;

import java.time.LocalDate;
import java.util.Iterator;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class, properties = {"input-file.path=./src/test/resources/fixtures/short_list.csv"})
class ApplicationIntegrationTest {

    @MockBean
    private DatesConsumer datesConsumer;
    @Captor
    private ArgumentCaptor<Iterator<LocalDate>> iterableArgumentCaptor;

    @Test
    public void processFile() {
        verify(datesConsumer).acceptAll(iterableArgumentCaptor.capture());

        assertThat(Lists.newArrayList(iterableArgumentCaptor.getValue()))
                .containsExactly(
                        LocalDate.of(2022, 7, 4),
                        LocalDate.of(2022, 7, 7),
                        LocalDate.of(2022, 7, 8)
                );

    }
}