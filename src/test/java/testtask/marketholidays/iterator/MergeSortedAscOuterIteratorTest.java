package testtask.marketholidays.iterator;

import com.google.common.collect.Lists;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.util.comparator.Comparators;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class MergeSortedAscOuterIteratorTest {
    private Comparator<Integer> comparator = Comparators.comparable();


    @ParameterizedTest(name = "{index} {0} left={1}, right={2}, expected={3}")
    @MethodSource("testCases")
    public void iterate(String testName, List<Integer> left, List<Integer> right, Integer[] expected) {
        assertThat(createIteratorAndCollectToList(
                left,
                right
        )).containsExactly(expected);

    }

    private static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of(
                        "Identical lists",
                        Arrays.asList(1, 2, 3),
                        Arrays.asList(1, 2, 3),
                        new Integer[] {}
                ),
                Arguments.of(
                        "Element missed in the middle on the right",
                        Arrays.asList(1, 2, 3),
                        Arrays.asList(1, 3),
                        new Integer[] {2}
                ),
                Arguments.of(
                        "Element missed in the middle on the left",
                        Arrays.asList(1, 3),
                        Arrays.asList(1, 2, 3),
                        new Integer[] {2}
                ),
                Arguments.of(
                        "Element missed at the begging on the right",
                        Arrays.asList(1, 2),
                        Arrays.asList(2),
                        new Integer[] {1}
                ),
                Arguments.of(
                        "Element missed at the begging on the left",
                        Arrays.asList(2),
                        Arrays.asList(1, 2),
                        new Integer[] {1}
                ),
                Arguments.of(
                        "Duplicate elements",
                        Arrays.asList(1, 2, 3),
                        Arrays.asList(1, 2, 2, 3),
                        new Integer[] {2}
                ),
                Arguments.of(
                        "Identical with duplicate elements",
                        Arrays.asList(1, 2, 2, 3),
                        Arrays.asList(1, 2, 2, 3),
                        new Integer[] {}
                ),
                Arguments.of(
                        "Left list is longer",
                        Arrays.asList(1, 2, 3),
                        Arrays.asList(1, 2),
                        new Integer[] {}
                ),
                Arguments.of(
                        "Right list is longer",
                        Arrays.asList(1, 2),
                        Arrays.asList(1, 2, 3),
                        new Integer[] {}
                ),
                Arguments.of(
                        "Complex case with several gaps on both sides",
                        Arrays.asList(/* 1, */ 2, 3, 4, 5, 6, 7, 8, 8, 8, 9, 10, 11, 12),
                        Arrays.asList(1, 2, 3, /* 4, */ 5, 6, 7, 8, /* 8, 8, */ 9),
                        new Integer[] {1, 4, 8, 8}
                )
        );
    }

    private List<Integer> createIteratorAndCollectToList(Iterable<Integer> left, Iterable<Integer> right) {
        return Lists.newArrayList(new MergeSortedAscOuterIterator<>(left.iterator(), right.iterator(), comparator));
    }

}